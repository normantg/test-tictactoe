<?php

return [
	/*
    |--------------------------------------------------------------------------
    | Used Database Connection 
    |--------------------------------------------------------------------------
    |
    | Here you can specify which database connection and configuration you want to use
    | from the configured connections below
    |
    */
	'default' => 'mockup',

	/*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | List of database connection with the configuration parameters for each one.
    |
    */
	'connections' => [
		'mockup' => [
			'host' 	=> getenv('MOCKUP_HOST') ?: 'local',
			'username' => getenv('MOCKUP_USER') ?: 'tic',
			'password' => getenv('MOCKUP_PASSWORD') ?: 'tac',
            'class' => 'Mockup'
		],
		'sqlite' => [
            'file'  => BASE_DIR.'/data/sqlite.db',
            'class' => 'Sqlite'
        ],
		'mysql' => []
	]
];
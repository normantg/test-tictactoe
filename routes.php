<?php

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


$routes = new RouteCollection();

	
$routes->add('create-user', new Route(
	'users', //path
	['_controller' => 'App\Controller\UsersController::createUser'], //Controller
	[], // requirements
	[], // options
	'', //host
	[], //schemes
	['POST'] //methods
));

$routes->add('delete-user', new Route(
	'users/{id}', //path
	['_controller' => 'App\Controller\UsersController::deleteUser'], //Controller
	[], // requirements
	[], // options
	'', //host
	[], //schemes
	['DELETE'] //methods
));

$routes->add('create-game', new Route(
	'games', //path
	['_controller' => 'App\Controller\GamesController::createGame'], //Controller
	[], // requirements
	[], // options
	'', //host
	[], //schemes
	['POST'] //methods
));

$routes->add('move-action-game', new Route(
	'games/{game_id}/move', //path
	['_controller' => 'App\Controller\GamesController::move'], //Controller
	[], // requirements
	[], // options
	'', //host
	[], //schemes
	['POST'] //methods
));

$routes->add('status-game', new Route(
	'games/{id}/status', //path
	['_controller' => 'App\Controller\GamesController::status'], //Controller
	[], // requirements
	[], // options
	'', //host
	[], //schemes
	['GET'] //methods
));

return $routes;
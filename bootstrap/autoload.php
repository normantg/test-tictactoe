<?php 

require __DIR__.'/../vendor/autoload.php';

define('BASE_DIR',__DIR__.'/../');

\App\Lib\Config::init(include(BASE_DIR.'/config/conf.php'));
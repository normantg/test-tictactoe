<?php

// Error reporting active in dev
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// require autoload
require __DIR__.'/../bootstrap/autoload.php';

// If command line executing is not correctly called, it will show a message
if (isset($argc) && $argc < 3) {
	echo "Usage: php index.php METHOD function (example: POST /users)\n";
	exit;
} 

$app = new App\Application();

// if command line arguments, set them in the application
if(isset($argv)) {
	$app->setArgv($argv);
}

// run the application
$app->run();

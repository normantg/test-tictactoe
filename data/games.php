<?php

return [
	[
		'id' => 1,
		'x' => 1,
		'o' => 2,
		'status' => 1, // active
		'winner' => 0
	],
	[
		'id' => 2,
		'x' => 1,
		'o' => 2,
		'status' => 0, // finished
		'winner' => 0
	],

];
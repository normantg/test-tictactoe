<?php

return [
	[
		'id' => 1,
		'game_id' => 1,
		'player' => 'x',
		'position_id' => 5
	],
	[
		'id' => 2,
		'game_id' => 1,
		'player' => 'o',
		'position_id' => 1
	],
	[
		'id' => 3,
		'game_id' => 1,
		'player' => 'x',
		'position_id' => 8
	],


	[
		'id' => 4,
		'game_id' => 2,
		'player' => 'x',
		'position_id' => 5
	],
	[
		'id' => 5,
		'game_id' => 2,
		'player' => 'o',
		'position_id' => 1
	],
	[
		'id' => 6,
		'game_id' => 2,
		'player' => 'x',
		'position_id' => 7
	],
	[
		'id' => 7,
		'game_id' => 2,
		'player' => 'o',
		'position_id' => 2
	],
	[
		'id' => 8,
		'game_id' => 2,
		'player' => 'x',
		'position_id' => 3
	]
];
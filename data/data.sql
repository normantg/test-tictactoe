create table users (
	id integer PRIMARY KEY,
	name varchar(64),
	email varchar(128)
);
create table games (
	id integer PRIMARY KEY,
	x integer not null,
	o integer not null,
	status integer not null default 1,
	winner integer default 0
);

create table moves (
	id integer PRIMARY KEY,
	game_id integer not null,
	player varchar(1) not null,
	position_id integer not null
);
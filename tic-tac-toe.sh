#!/bin/bash

rm data/sqlite.db
sqlite3 data/sqlite.db < data/data.sql

echo "------------- USERS -------------"
echo "executing: php public/index.php POST /users/?name=Norman&email=normantg@gmail.com"
php public/index.php POST "/users?name=Norman&email=normantg@gmail.com"
echo "\n"

echo "executing: php public/index.php POST /users/?name=Example&email=example@example.com"
php public/index.php POST "/users?name=Example&email=example@example.com"
echo "\n"

echo "executing: php public/index.php POST /users/?name=Example2&email=example2@example.com"
php public/index.php POST "/users?name=Example2&email=example2@example.com"
echo "\n"

echo "executing: php public/index.php DELETE /users//3"
php public/index.php DELETE "/users/3"
echo "\n"


echo "------------- GAMES -------------"
echo "executing: php public/index.php POST /game?x=1&o=2"
php public/index.php POST "/games?x=1&o=2"
echo "\n"

echo "executing: php public/index.php POST /games/1/move?player=x&position_id=5"
php public/index.php POST "/games/1/move?player=x&position_id=5"
echo "\n"

echo "executing: php public/index.php GET /games/1/status"
php public/index.php GET "/games/1/status"
echo "\n"

echo "executing: php public/index.php POST /games/1/move?player=o&position_id=1"
php public/index.php POST "/games/1/move?player=o&position_id=1"
echo "\n"

echo "executing: php public/index.php GET /games/1/status"
php public/index.php GET "/games/1/status"
echo "\n"

echo "executing: php public/index.php POST /games/1/move?player=x&position_id=7"
php public/index.php POST "/games/1/move?player=x&position_id=7"
echo "\n"

echo "executing: php public/index.php GET /games/1/status"
php public/index.php GET "/games/1/status"
echo "\n"

echo "executing: php public/index.php POST /games/1/move?player=o&position_id=2"
php public/index.php POST "/games/1/move?player=o&position_id=2"
echo "\n"

echo "executing: php public/index.php GET /games/1/status"
php public/index.php GET "/games/1/status"
echo "\n"

echo "executing: php public/index.php POST /games/1/move?player=x&position_id=3"
php public/index.php POST "/games/1/move?player=x&position_id=3"
echo "\n"

echo "executing: php public/index.php GET /games/1/status"
php public/index.php GET "/games/1/status"
echo "\n"

<?php

namespace App;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;


class Application
{

	private $routes = array();
	private $argv;

	public function __construct()
	{
		$this->routes = include __DIR__.'/../routes.php';
	}


	/**
	 * Run the application - It will handle the request differently if the code is executed 
	 * as http request or via command line
	 */
	public function run()
	{
		$request = Request::createFromGlobals();

		try {
			if(isset($this->argv)) {
				echo $response = $this->handleCommand($request);
			} else {
				echo $response = $this->handle($request);
			}
		} catch(Exception $e) {
			$response = $e->getMessage();
		}
	}

	/**
	 * Handle the request when executed via command line
	 *
	 * @param Request $request Request
	 *
	 * @return Response response
	 */
	public function handleCommand(Request $request)
	{
		// Retrieve the command line arguments
		$method = $this->argv[1];
		$action = explode('?',$this->argv[2]);
		$function = $action[0];
		$args = isset($action[1]) ? explode('&',$action[1]) : [];
		
		// Set the request with the arguments
		$request = $this->addCommandQuery($request,$args);
		$request->setMethod($method);
		
		$context = new RequestContext('/');
		$context->setMethod($method);
		$context->setPathInfo($function);
		
		// Match the URL with the routes
		$matcher = new UrlMatcher($this->routes, $context);
		$parameters = $matcher->match($function);
		$request->attributes->add($parameters);

		$controllerResolver = new ControllerResolver();
		$argumentsResolver = new ArgumentResolver();
		
		// Call the designated controller		
		$controller = $controllerResolver->getController($request);
		$arguments = $argumentsResolver->getArguments($request,$controller);
		$response = call_user_func_array($controller,$arguments);

		return json_encode($response);
	}

	/**
	 * Handle the request when called via http
	 *
	 * @param Request $request Request
	 *
	 * @return Response response
	 */
	public function handle(Request $request)
	{
		$context = new RequestContext('/');
		$context->setMethod($request->getMethod());

		$matcher = new UrlMatcher($this->routes, $context);

		$parameters = $matcher->match($request->getPathInfo());
		$request->attributes->add($parameters);

		$controllerResolver = new ControllerResolver();
		$argumentsResolver = new ArgumentResolver();
		$controller = $controllerResolver->getController($request);
	
		$arguments = $argumentsResolver->getArguments($request,$controller);
		$response = call_user_func_array($controller,$arguments);

		return json_encode($response);
	}

	/**
	 * Extract the arguments in the command line query URL and set them in the request
	 *
	 * @param Request $request Request
	 * @param array $args command line arguments
	 *
	 * @return Request request with added arguments
	 */
	public function addCommandQuery(Request $request, $args)
	{
		foreach($args as $arg) {
			$param = explode('=',$arg);
			$request->attributes->set($param[0],$param[1]);
		}
		return $request;
	}

	/**
	 * Set a command line argument if the application is run from the command line
	 *
	 * @param array $argv command line arguments
	 */
	public function setArgv($argv)
	{
		$this->argv = $argv;
	}
}
<?php 

namespace App\Lib\Databases;

use App\Lib\Config;
use App\Lib\Databases\DatabasesInterface;

class Sqlite implements DatabasesInterface
{
	private $dataset;
	private $db;

	public function __construct()
	{
		$default_connection = Config::get('databases.default');
		$connection = Config::get('databases.connections.'.$default_connection);
		$this->db = new \SQLite3($connection['file']);
	}

	/**
	 * Returns a list with all the data
	 *	 
	 * @return array list with all the data
	 */
	public function fetchAll()
	{
		return $this->dataset;
	}

	/**
	 * Returns the filtered elements by key and value
	 *	 
	 * @param string $key key to filter from
	 * @param string $value value to filter from
	 *
	 * @return array elements with all its objects as an associative array
	 */
	public function fetchByKey($key,$value)
	{
		$query = "SELECT * FROM $this->dataset WHERE $key = '$value'";
		$results = $this->db->query($query);	
		$data = [];	
		while ($row = $results->fetchArray()) {
    		$data[] = $row;
		}
		return $data;
	}

	/**
	 * Insert data in to the system
	 *	 
	 * @param array $data data to be inserted in the system
	 *
	 * @return array inseted data
	 */
	public function insert($data)
	{
		unset($data['id']);
		$keys = implode(',',array_keys($data));
		$values = '"'.implode('","',$data).'"';
		$query = "INSERT INTO $this->dataset ($keys) values ($values)";

		$results = $this->db->query($query);

		$id = $this->db->lastInsertRowID();

		return $this->fetchByKey('id',$id);
	}

	/**
	 * Updates data into the system
	 *	 
	 * @param string $key key to filter from
	 * @param string $value value to filter from
	 * @param array $data to be updated
	 *
	 * @return array updated elements with all its objects as an associative array
	 */
	public function update($key, $value, $data)
	{
		foreach($data as $k => $v) {
			$data[$k] = "$k = '$v'";
		}
		$set = implode(', ',$data);
		$query = "UPDATE $this->dataset SET $set WHERE $key = '$value'";
		$this->db->query($query);

		return $this->fetchByKey($key,$value);
	}

	/**
	 * Deletes the data filtered by ID
	 *	 
	 * @param integer $id data ID
	 *
	 * @return boolean
	 */
	public function delete($id)
	{
		$query = "DELETE FROM $this->dataset WHERE id = $id";
		$this->db->query($query);
		return true;
	}

	/**
	 * Sets which dataset the class has to look
	 *	 
	 * @param string $name dataset name
	 */
	public function setDataset($name)
	{
		$this->dataset = $name;
	}
}
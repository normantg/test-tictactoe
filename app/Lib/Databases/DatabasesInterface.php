<?php

namespace App\Lib\Databases;

interface DatabasesInterface 
{
	// Returns a list with all the data
	public function fetchAll();

	// Returns the filtered elements by key and value
	public function fetchByKey($key, $value);

	// Insert data in to the system
	public function insert($data);

	// Updates data into the system
	public function update($key, $value, $data);

	// Deletes the data filtered by ID
	public function delete($id);
}
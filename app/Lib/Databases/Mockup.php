<?php 

namespace App\Lib\Databases;

use App\Lib\Databases\DatabasesInterface;

class Mockup implements DatabasesInterface
{
	private $dataset;

	public function __construct()
	{
		
	}

	/**
	 * Returns a list with all the data
	 *	 
	 * @return array list with all the data
	 */
	public function fetchAll()
	{
		return $this->dataset;
	}

	/**
	 * Returns the filtered elements by key and value
	 *	 
	 * @param string $key key to filter from
	 * @param string $value value to filter from
	 *
	 * @return array elements with all its objects as an associative array
	 */
	public function fetchByKey($key,$value)
	{
		return array_filter($this->dataset,function($data) use ($key,$value) {
			return $data[$key] == $value;
		});
	}

	/**
	 * Insert data in to the system
	 *	 
	 * @param array $data data to be inserted in the system
	 *
	 * @return array inseted data
	 */
	public function insert($data)
	{
		// for this example the same data with a random mock-up id is returned
		$data['id'] = 123;
		return $data;
	}

	/**
	 * Updates data into the system
	 *	 
	 * @param string $key key to filter from
	 * @param string $value value to filter from
	 * @param array $data to be updated
	 *
	 * @return array updated elements with all its objects as an associative array
	 */
	public function update($key, $value, $data)
	{
		$rows = array_filter($this->dataset,function($data) use ($key,$value) {
			return $data[$key] == $value;
		});

		foreach($rows as &$row) {
			$row = array_merge($row,$data);

		}
		
		return $rows;
	}

	/**
	 * Deletes the data filtered by ID
	 *	 
	 * @param integer $id data ID
	 *
	 * @return boolean
	 */
	public function delete($id)
	{
		// for this example, we will asume the data is always deleted and true is returned
		return true;
	}

	/**
	 * Sets which dataset the class has to look
	 *	 
	 * @param string $name dataset name
	 */
	public function setDataset($name)
	{
		$this->dataset = include(BASE_DIR.'/data/'.$name.'.php');
	}
}
<?php

namespace App\Lib;

use Arrayy\Arrayy as A;

class Config
{
	private static $conf;

	/**
	 * Sets the configuration Array
	 *
	 * @param array $conf configuration Array
	 */
	public static function init($conf)
	{
		Self::$conf = $conf;
	}

	/**
	 * Set the configuration value for the related key
	 *
	 * @param string $key key in 'dot' format to access the conf array
	 * @param string $value value to set in the configuration
	 */
	public static function set($key, $value)
	{
		$config = new A(Self::$conf);
		$config->set($key,$value);
	}

	/**
	 * Returns the configuration array related with the provided key
	 *
	 * @param string $key key in 'dot' format to access the conf array
	 *
	 * @return array Configuration array
	 */
	public static function get($key)
	{
		$config = new A(Self::$conf);
		return $config->get($key);
	}
}
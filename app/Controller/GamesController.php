<?php 

namespace App\Controller;

use App\Model\Games;
use App\Model\Moves;
use Symfony\Component\HttpFoundation\Request;

class GamesController
{
	private $games;
	private $moves;
	const FINISHED = 0; 
	
	public function __construct()
	{
		$this->games = new Games();
		$this->moves = new Moves();
	}

	/**
	 * Starts a new game between 2 users
	 *
	 * @param Request $request
	 *
	 * @return array data of the started game
	 */

	public function createGame(Request $request)
	{
		// Sanitize input data
		$data = $this->filterGameData($request);

		$game = $this->games->createGame($data);

		return $game;		
	}

	/**
	 * Register a move made by a player. Also checks if the player wins with the current move
	 *
	 * @param Request $request
	 *
	 * @return array data of move made
	 */
	public function move(Request $request)
	{
		// Sanitize input data
		$data = $this->filterMoveData($request);

		// Make a move
		$move = $this->moves->makeMove($data);

		// Update the game status
		$game = $this->udpateGameStatus($data['game_id']);

		return $move;
	}

	/**
	 * Returns the game status
	 *
	 * @param Request $request
	 *
	 * @return array finished information (true/false) and how is the winner
	 */
	public function status(Request $request)
	{
		// Sanitize input data
		$data = $this->filterGameData($request);

		return $this->isGameFinished($data['id']);
	}

	/**
	 * Check the moves of a game and computes if there is a winner. If there is a winner, the Game 
	 * data is updated
	 *
	 * @param integer $game_id
	 *
	 * @return boolean true/false if game has been updated
	 */
	protected function udpateGameStatus($game_id)
	{
		$moves = $this->moves->getGameMoves($game_id);

		$winner = $this->computeWinner($moves);
		
		if($winner) {
			// Get the game information in order to know the player ID of the winner (X or O)
			$game = $this->games->getGame($game_id);
			$data = [
				'status' => Self::FINISHED,
				'winner' => $game[$winner] //player ID
			];
			$game = $this->games->updateGame($game_id,$data);
			return true;
		}

		return false;
	}

	/**
	 * Checks if there is a winner acording to the moves in the game
	 *
	 * @param array $moves
	 *
	 * @return mixed player who has won. False otherwise.
	 */
	protected function computeWinner($moves)
	{
		// fill the board with X o O according to the registered moves
		$board = array_fill(1,9,null);
		foreach($moves as $move) {
			$board[$move['position_id']] = $move['player'];
		}

		if($winner = $this->checkRows($board)) {
			return $winner;
		} else if($winner = $this->checkColumns($board)) {
			return $winner;
		} else if($winner = $this->checkDiagonals($board)) {
			return $winner;
		} else {
			return false;
		}
	}

	/**
	 * Check the rows combinations for a winner
	 *
	 * @param array $board
	 *
	 * @return mixed player who has one. False otherwise.
	 */
	protected function checkRows($board)
	{
		$init_rows = [1,4,7];
		foreach($init_rows as $row) {
			if($board[$row] === $board[$row+1] && $board[$row] === $board[$row+2]) {
				return $board[$row];
			}
		}
		return false;
	}

	/**
	 * Check the columns combinations for a winner
	 *
	 * @param array $board
	 *
	 * @return mixed player who has one. False otherwise.
	 */
	protected function checkColumns($board)
	{
		$init_columns = [1,2,3];
		foreach($init_columns as $row) {
			if($board[$row] === $board[$row+3] && $board[$row] === $board[$row+6]) {
				return $board[$row];
			}
		}
		return false;
	}

	/**
	 * Check the diagonal combinations for a winner
	 *
	 * @param array $board
	 *
	 * @return mixed player who has one. False otherwise.
	 */
	protected function checkDiagonals($board)
	{
		if($board[1] === $board[5] && $board[1] === $board[9]) { return $board[1]; }
		if($board[3] === $board[5] && $board[3] === $board[7]) { return $board[3]; }
		return false;
	}

	/**
	 * Check the if the game has finished. If it has finished, it also returns who is the winner.
	 *
	 * @param integer $game_id
	 *
	 * @return array if the game has finished and who is the winner
	 */
	protected function isGameFinished($game_id)
	{
		$game = $this->games->getGame($game_id);

		return [
			'finished' => $game['status'] == 'finished' ? true : false,
			'winner' => $game['winner']
		];
	}

	/**
	 * Sanitizes the data needed in order to avoid out of regular information from the input.
	 *
	 * @param Request $request
	 *
	 * @return array user parameters
	 */
	protected function filterGameData(Request $request)
	{
		$data = [
			'id' => $request->get('id'),
			'x' => $request->get('x'),
			'o' => $request->get('o')
		];

		$args = [
			'id' => FILTER_SANITIZE_NUMBER_INT,
			'x' => FILTER_SANITIZE_NUMBER_INT,
			'o' => FILTER_SANITIZE_NUMBER_INT
		];

		$sanitized = filter_var_array($data,$args);
		return $sanitized;
	}

	/**
	 * Sanitizes the data needed in order to avoid out of regular information from the input.
	 *
	 * @param Request $request
	 *
	 * @return array user parameters
	 */
	protected function filterMoveData(Request $request)
	{
		$data = [
			'game_id' => $request->get('game_id'),
			'player' => $request->get('player'),
			'position_id' => $request->get('position_id')
		];

		$args = [
			'game_id' => FILTER_SANITIZE_NUMBER_INT,
			'player' => FILTER_SANITIZE_STRING,
			'position_id' => FILTER_SANITIZE_NUMBER_INT
		];

		$sanitized = filter_var_array($data,$args);
		return $sanitized;
	}

}
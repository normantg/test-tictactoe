<?php

namespace App\Controller;

use App\Model\Users;
use Symfony\Component\HttpFoundation\Request;

class UsersController
{
	private $users;

	public function __construct()
	{
		$this->users = new Users();
	}
	/**
	 * Creates a user. Verifies if the user exists first. If the user already exists, it returns the 
	 * user with a message.
	 *
	 * @param Request $request
	 *
	 * @return array data of the created user
	 */

	public function createUser(Request $request)
	{
		// Sanitize input data
		$data = $this->filterUserData($request);

		$user = $this->users->getUser('email',$data['email']);
		
		if(empty($user)) {
			// If user does not exist
			return $this->users->createUser($data);
		} else {
			// If user exists
			return array_merge(
				['message' => 'User already existed'],
				reset($user)
			);
		}
	}

	/**
	 * Deletes a user. It deletes a user if exists. It will return an array with a status = true/false
	 *
	 * @param Request $request
	 *
	 * @return array with status true/false
	 */
	public function deleteUser(Request $request)
	{
		// Sanitize input data
		$data = $this->filterUserData($request);

		$result = $this->users->deleteUser($data['id']);
		return [
			'status' => $result
		];
	}

	/**
	 * Sanitizes the data needed for the user in order to avoid out of regular information from the input.
	 *
	 * @param Request $request
	 *
	 * @return array user parameters
	 */
	protected function filterUserData(Request $request)
	{
		$data = [
			'id' => $request->get('id'),
			'name' => $request->get('name'),
			'email' => $request->get('email')
		];

		$args = [
			'id' => FILTER_SANITIZE_NUMBER_INT,
			'name' => FILTER_SANITIZE_STRING,
			'email' => FILTER_SANITIZE_EMAIL
		];

		$sanitized = filter_var_array($data,$args);
		return $sanitized;
	}

}
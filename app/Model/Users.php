<?php

namespace App\Model;

use App\Model\Interfaces\UsersInterface;
use App\Lib\Config;


class Users implements UsersInterface 
{
	private $db; // database

	/**
	 * Sets the database class to use and its dataset
	 */
	public function __construct()
	{
		// Instanciate class
		$default_connection = Config::get('databases.default');
		$connection = Config::get('databases.connections.'.$default_connection);
		$class = 'App\\Lib\\Databases\\'.$connection['class'];
		$this->db = new $class();

		// Select dataset
		$this->db->setDataset('users');
	}

	/**
	 * Returns a list with all the users
	 *	 
	 * @return array list with all the users
	 */

	public function getUsers() 
	{
		return $this->db->fetchAll();
	}

	/**
	 * Returns the users filtered by the key and value
	 *	 
	 * @param string $key key to filter from
	 * @param string $value value to filter from
	 *
	 * @return array user with all its objects as an associative array
	 */
	public function getUser($key, $value) 
	{
		return $this->db->fetchByKey($key,$value);
	}

	/**
	 * Creates a user in the system
	 *	 
	 * @param array $data user data to store in the system
	 *
	 * @return array stored user with all its objects as an associative array
	 */
	public function createUser($data) 
	{
		return $this->db->insert($data);	
	}

	/**
	 * Updates the data for a single user (this method has not been implemented yet, since is not needed right now)
	 *	 
	 * @param integer $id user unique ID
	 * @param array $data user data to update in the system
	 *
	 * @return array user with all its objects as an associative array
	 */
	public function updateUser($id, $data) 
	{

	}

	/**
	 * Deletes the user from the system
	 *	 
	 * @param integer $id user unique ID
	 *
	 * @return boolean true if the deletion has been correct
	 */
	public function deleteUser($id) 
	{
		return $this->db->delete($id);
	}
	
}
<?php

namespace App\Model\Interfaces;

interface GamesInterface 
{

	// Returns a list with all the games
	public function getGames();

	// Returns information about a game
	public function getGame($game_id);

	// Returns all the games from a user
	public function getGamesByUser($id, $status);

	// Creates a game into the system
	public function createGame($data);

	// Updates the data for a single game
	public function updateGame($id, $data);

}
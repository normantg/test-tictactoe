<?php

namespace App\Model\Interfaces;

interface UsersInterface 
{

	// Returns a list with all the users
	public function getUsers();

	// Returns the users filtered by the key and value
	public function getUser($key, $value);

	// Creates a user in the system
	public function createUser($data);

	// Updates the data for a single user
	public function updateUser($id, $data);

	// Deletes the user from the system
	public function deleteUser($id);
}
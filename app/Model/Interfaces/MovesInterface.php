<?php

namespace App\Model\Interfaces;

interface MovesInterface 
{

	// Returns a list with all the moves of a single game
	public function getGameMoves($game_id);

	// Returns all the player moves for a single game
	public function getGamePlayerMoves($player_id, $game_id);

	// Inserts a move into the game
	public function makeMove($data);

}
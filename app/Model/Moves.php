<?php

namespace App\Model;

use App\Model\Interfaces\MovesInterface;
use App\Lib\Config;


class Moves implements MovesInterface 
{
	private $db; // database

	/**
	 * Sets the database class to use and its dataset
	 */
	public function __construct()
	{
		// Instanciate class
		$default_connection = Config::get('databases.default');
		$connection = Config::get('databases.connections.'.$default_connection);
		$class = 'App\\Lib\\Databases\\'.$connection['class'];
		$this->db = new $class();

		// Select dataset
		$this->db->setDataset('moves');
	}

	/*
	 * Returns a list with all the moves of a single game
	 *
	 * @param integer $game_id
	 *
	 * @return array
	 */
	public function getGameMoves($game_id)
	{
		return $this->db->fetchByKey('game_id',$game_id);
	}

	/*
	 * Returns all the player moves for a single game
	 *
	 * @param integer $player_id
	 * @param integer $game_id
	 *
	 * @return array
	 */
	public function getGamePlayerMoves($player_id, $game_id)
	{
		// not implemented yet
		return [];
	}

	/**
	 * Inserts a move into the game
	 *	 
	 * @param array $data move data to store in the system
	 *
	 * @return array stored move data with all its objects as an associative array
	 */
	public function makeMove($data)
	{
		return $this->db->insert($data);
	}
}


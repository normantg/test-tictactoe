<?php

namespace App\Model;

use App\Model\Interfaces\GamesInterface;
use App\Lib\Config;


class Games implements GamesInterface 
{
	private $db; // database

	/**
	 * Sets the database class to use and its dataset
	 */
	public function __construct()
	{
		// Instanciate class
		$default_connection = Config::get('databases.default');
		$connection = Config::get('databases.connections.'.$default_connection);
		$class = 'App\\Lib\\Databases\\'.$connection['class'];
		$this->db = new $class();

		// Select dataset
		$this->db->setDataset('games');
	}

	/*
	 * Returns a list with all the games
	 *
	 * @return array
	 */
	public function getGames()
	{
		// has not been implemented yet
		return [];
	}

	/*
	 * Returns information about a game
	 *
	 * @param integer $game_id
	 *
	 * @return array
	 */
	public function getGame($game_id)
	{
		$games = $this->db->fetchByKey('id',$game_id);
		return reset($games);
	}

	/*
	 * Returns all the games from a user
	 *
	 * @param integer $id
	 * @param string $status finished or not
	 *
	 * @return array
	 */
	public function getGamesByUser($id, $status)
	{
		// has not been implemented yet
		return [];
	}

	/*
	 * Returns all the games from a user
	 *
	 * @param array $data game data to store in the system
	 *
	 * @return array stored game with all its objects as an associative array
	 */
	public function createGame($data)
	{
		return $this->db->insert($data);
	}

	/**
	 * Updates the data for a single game
	 *	 
	 * @param integer $id game unique ID
	 * @param array $data game data to update in the system
	 *
	 * @return array game with all its objects as an associative array
	 */
	public function updateGame($id, $data)
	{
		return $this->db->update('id',$id,$data);
	}
}

